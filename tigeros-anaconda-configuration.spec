Name:           tigeros-anaconda-configuration
Version:        1.0
Release:        1%{?dist}
Summary:        TigerOS Anaconda Configuration

License:        GPLv2+
URL:            https://gitlab.com/RITlug/TigerOS/tigeros-packages/tigeros-anaconda-configuration
Source0:        tigeros.conf

BuildRequires:  anaconda-core

Requires:       anaconda-core
Supplements:    (tigeros-release and anaconda-core)

%description
This package contains the TigerOS anaconda configuration.


%prep
# Nothing to prep


%build
# Nothing to build


%install
install -Dpm 0644 %{SOURCE0} %{_sysconfdir}/anaconda/product.d/tigeros.conf


%files
%{_sysconfdir}/anaconda/product.d/tigeros.conf


%changelog
* Mon Jul 29 2019 Tim Zabel <tjz8659@rit.edu> 1.0-1
- Fedora 30 initial build
- Fix description
